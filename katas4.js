const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

kata1();
kata2();
kata3();
kata4();
kata5();
kata6();
kata7();
kata8();
kata9();
kata10();
kata11();
kata12();
kata13();
kata14();
kata15();
kata16();
kata17();
kata18();
kata19();
kata20();
kata21();
kata22();
kata23();
kata24();
kata25();
kata26();
kata27();
kata28();
kata29();
kata30();
kata31();
kata32();

function isVowel(letter){
    if(letter === "a" || letter === "e" || letter === "i" || letter === "o" || letter === "u" || letter === "A" || letter === "E" || letter === "I" || letter === "O" || letter === "U"){
        return true;
    }
    return false;
}
function kata1() {
    let header = document.createElement("div");
    header.textContent = "Kata 1";
    document.body.appendChild(header);

    let arr = gotCitiesCSV.split(",");
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(arr);
    document.body.appendChild(newElement)
    return arr; 
}
function kata2() {
    let header = document.createElement("div");
    header.textContent = "Kata 2";
    document.body.appendChild(header);

    let arr = bestThing.split(" ");
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(arr);
    document.body.appendChild(newElement)
    return arr; 
}
function kata3() {
    let header = document.createElement("div");
    header.textContent = "Kata 3";
    document.body.appendChild(header);

    let str = gotCitiesCSV.replace(/,/g, ";");
    let newElement = document.createElement("div");
    newElement.textContent = str;
    document.body.appendChild(newElement)
    return str; 
}
function kata4() {
    let header = document.createElement("div");
    header.textContent = "Kata 4";
    document.body.appendChild(header);

    let str = lotrCitiesArray.join(",");
    let newElement = document.createElement("div");
    newElement.textContent = str;
    document.body.appendChild(newElement)
    return str; 
}
function kata5() {
    let header = document.createElement("div");
    header.textContent = "Kata 5";
    document.body.appendChild(header);

    let arr = [];
    for(let i = 0; i < 5; i ++){
        arr.push(lotrCitiesArray[i]);
    }
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(arr);;
    document.body.appendChild(newElement)
    return arr; 
}
function kata6() {
    let header = document.createElement("div");
    header.textContent = "Kata 6";
    document.body.appendChild(header);

    let arr = [];
    for(let i = lotrCitiesArray.length - 1; i > (lotrCitiesArray.length - 6); i --){
        arr.push(lotrCitiesArray[i]);
    }
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(arr);;
    document.body.appendChild(newElement)
    return arr; 
}
function kata7() {
    let header = document.createElement("div");
    header.textContent = "Kata 7";
    document.body.appendChild(header);

    let arr = [];
    for(let i = 2; i < 5; i ++){
        arr.push(lotrCitiesArray[i]);
    }
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(arr);
    document.body.appendChild(newElement)
    return arr;  
}
function kata8() {
    let header = document.createElement("div");
    header.textContent = "Kata 8";
    document.body.appendChild(header);

    lotrCitiesArray.splice(2, 1);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; 
}
function kata9() {
    let header = document.createElement("div");
    header.textContent = "Kata 9";
    document.body.appendChild(header);

    lotrCitiesArray.splice(5, 5);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; 
}
function kata10() {
    let header = document.createElement("div");
    header.textContent = "Kata 10";
    document.body.appendChild(header);

    lotrCitiesArray.splice(2, 0, "Rohan");
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray;
}
function kata11() {
    let header = document.createElement("div");
    header.textContent = "Kata 11";
    document.body.appendChild(header);

    lotrCitiesArray.splice(5, 1, "Deadest Marshes");
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray;
}
function kata12() {
    let header = document.createElement("div");
    header.textContent = "Kata 12";
    document.body.appendChild(header);

    let str = bestThing.slice(0, 15);
    let newElement = document.createElement("div");
    newElement.textContent = str;
    document.body.appendChild(newElement)
    return str; 
}
function kata13() {
    let header = document.createElement("div");
    header.textContent = "Kata 13";
    document.body.appendChild(header);

    let str = bestThing.slice(bestThing.length - 12, bestThing.length);
    let newElement = document.createElement("div");
    newElement.textContent = str;
    document.body.appendChild(newElement)
    return str; 
}
function kata14() {
    let header = document.createElement("div");
    header.textContent = "Kata 14";
    document.body.appendChild(header);

    let str = bestThing.slice(23, 38);
    let newElement = document.createElement("div");
    newElement.textContent = str;
    document.body.appendChild(newElement)
    return str; 
}
function kata15() {
    let header = document.createElement("div");
    header.textContent = "Kata 15";
    document.body.appendChild(header);

    let str = bestThing.substring(bestThing.length - 12, bestThing.length);
    let newElement = document.createElement("div");
    newElement.textContent = str;
    document.body.appendChild(newElement)
    return str; 
}
function kata16() {
    let header = document.createElement("div");
    header.textContent = "Kata 16";
    document.body.appendChild(header);

    let str = bestThing.substring(23, 38);
    let newElement = document.createElement("div");
    newElement.textContent = str;
    document.body.appendChild(newElement)
    return str; 
}
function kata17() {
    let header = document.createElement("div");
    header.textContent = "Kata 17";
    document.body.appendChild(header);

    let num = bestThing.indexOf("only")
    let newElement = document.createElement("div");
    newElement.textContent = num;
    document.body.appendChild(newElement)
    return num; 
}
function kata18() {
    let header = document.createElement("div");
    header.textContent = "Kata 18";
    document.body.appendChild(header);

    let num = bestThing.lastIndexOf(" ") + 1;
    let newElement = document.createElement("div");
    newElement.textContent = num;
    document.body.appendChild(newElement)
    return num; 
}
function kata19() {
    let header = document.createElement("div");
    header.textContent = "Kata 19";
    document.body.appendChild(header);

    let arr = gotCitiesCSV.split(","), previosLetter = 0, retArr = [];
    for(let i in arr){
        for(let j in arr[i]){
            if(isVowel(arr[i].charAt(j))){
                if(previosLetter == 1){
                    retArr.push(arr[i]);
                    break;
                }
                else
                    previosLetter = 1;
            }
            else{
                previosLetter = 0;
            }
        }
    }
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(retArr);
    document.body.appendChild(newElement)
    return retArr; 
}
function kata20() {
    let header = document.createElement("div");
    header.textContent = "Kata 20";
    document.body.appendChild(header);

    let arr = lotrCitiesArray, retArr = [];
    for(let i in arr){
        if(arr[i].charAt(arr[i].length - 2) === "o" && arr[i].charAt(arr[i].length - 1) === "r")
            retArr.push(arr[i]);
    }
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(retArr);
    document.body.appendChild(newElement)
    return retArr;  
}
function kata21() {
    let header = document.createElement("div");
    header.textContent = "Kata 21";
    document.body.appendChild(header);

    let arr = bestThing.split(" "), retArr = [];
    for(let i in arr)
        if(arr[i].charAt(0) === "b" || arr[i].charAt(0) === "B")
            retArr.push(arr[i]);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(retArr);
    document.body.appendChild(newElement)
    return retArr; 
}
function kata22() {
    let header = document.createElement("div");
    header.textContent = "Kata 22";
    document.body.appendChild(header);

    let retString = "";
    if(lotrCitiesArray.indexOf("Mirkwood") == -1)
        retString = "No";
    else    
        retString = "Yes"
    let newElement = document.createElement("div");
    newElement.textContent = retString;
    document.body.appendChild(newElement)
    return retString; 
}
function kata23() {
    let header = document.createElement("div");
    header.textContent = "Kata 23";
    document.body.appendChild(header);

    let retString = "";
    if(lotrCitiesArray.indexOf("Hollywood") == -1)
        retString = "No";
    else    
        retString = "Yes"
    let newElement = document.createElement("div");
    newElement.textContent = retString;
    document.body.appendChild(newElement)
    return retString; 
}
function kata24() {
    let header = document.createElement("div");
    header.textContent = "Kata 24";
    document.body.appendChild(header);

    let num = lotrCitiesArray.indexOf("Mirkwood");
    let newElement = document.createElement("div");
    newElement.textContent = num;
    document.body.appendChild(newElement)
    return num; 
}
function kata25() {
    let header = document.createElement("div");
    header.textContent = "Kata 25";
    document.body.appendChild(header);

    let str = "";
    for(let i in lotrCitiesArray)
        if(lotrCitiesArray[i].indexOf(" ") != -1){
            str = lotrCitiesArray[i];
            break;
        }
    let newElement = document.createElement("div");
    newElement.textContent = str;
    document.body.appendChild(newElement)
    return str; 
}
function kata26() {
    let header = document.createElement("div");
    header.textContent = "Kata 26";
    document.body.appendChild(header);

    lotrCitiesArray.reverse();
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; 
}
function kata27() {
    let header = document.createElement("div");
    header.textContent = "Kata 27";
    document.body.appendChild(header);

    lotrCitiesArray.sort();
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; 
}
function kata28() {
    let header = document.createElement("div");
    header.textContent = "Kata 28";
    document.body.appendChild(header);

    lotrCitiesArray.sort(function(a, b){return a.length - b.length});
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; 
}
function kata29() {
    let header = document.createElement("div");
    header.textContent = "Kata 29";
    document.body.appendChild(header);

    lotrCitiesArray.pop();
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; 
}
function kata30() {
    let header = document.createElement("div");
    header.textContent = "Kata 30";
    document.body.appendChild(header);

    lotrCitiesArray.push("Deadest Marshes");
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; 
}
function kata31() {
    let header = document.createElement("div");
    header.textContent = "Kata 31";
    document.body.appendChild(header);

    lotrCitiesArray.shift();
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; 
}
function kata32() {
    let header = document.createElement("div");
    header.textContent = "Kata 32";
    document.body.appendChild(header);

    lotrCitiesArray.unshift("Rohan");
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; 
}